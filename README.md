# SPTiOSLibrary

[![CI Status](http://img.shields.io/travis/maheshbabu.somineni@gmail.com/SPTiOSLibrary.svg?style=flat)](https://travis-ci.org/maheshbabu.somineni@gmail.com/SPTiOSLibrary)
[![Version](https://img.shields.io/cocoapods/v/SPTiOSLibrary.svg?style=flat)](http://cocoapods.org/pods/SPTiOSLibrary)
[![License](https://img.shields.io/cocoapods/l/SPTiOSLibrary.svg?style=flat)](http://cocoapods.org/pods/SPTiOSLibrary)
[![Platform](https://img.shields.io/cocoapods/p/SPTiOSLibrary.svg?style=flat)](http://cocoapods.org/pods/SPTiOSLibrary)

## Example

To run the example project, clone the repo using following pod name.

 pod 'SPTiOSLibrary', :git => 'https://maheshbabusomineni@bitbucket.org/maheshbabusomineni/sptioslibrary.git', :branch => 'master'

## Requirements

## Installation

SPTiOSLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

pod 'SPTiOSLibrary', :git => 'https://maheshbabusomineni@bitbucket.org/maheshbabusomineni/sptioslibrary.git', :branch => 'master'



## Author

Maheshbabu Somineni, maheshbabu.somineni@gmail.com

## License

SPTiOSLibrary is available under the MIT license. See the LICENSE file for more info.
